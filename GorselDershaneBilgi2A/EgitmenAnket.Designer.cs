﻿namespace GorselDershaneBilgi2A
{
    partial class EgitmenAnket
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblEgitmenAdi = new System.Windows.Forms.Label();
            this.lblEgitmenSoyadi = new System.Windows.Forms.Label();
            this.lblSinif = new System.Windows.Forms.Label();
            this.llblbrans = new System.Windows.Forms.Label();
            this.lblYorum = new System.Windows.Forms.Label();
            this.cmbbrans = new System.Windows.Forms.ComboBox();
            this.txtEgitmanAd = new System.Windows.Forms.TextBox();
            this.txtEgitmenSoyadi = new System.Windows.Forms.TextBox();
            this.txtYorum = new System.Windows.Forms.TextBox();
            this.txtSinif = new System.Windows.Forms.TextBox();
            this.lblSoru3 = new System.Windows.Forms.Label();
            this.lblSoru11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblSoru12 = new System.Windows.Forms.Label();
            this.chckEvet1 = new System.Windows.Forms.CheckBox();
            this.chckHayır1 = new System.Windows.Forms.CheckBox();
            this.chckEvet2 = new System.Windows.Forms.CheckBox();
            this.chckHayır2 = new System.Windows.Forms.CheckBox();
            this.chckEvet3 = new System.Windows.Forms.CheckBox();
            this.chckHayır3 = new System.Windows.Forms.CheckBox();
            this.lblSoru13 = new System.Windows.Forms.Label();
            this.lblSoru1 = new System.Windows.Forms.Label();
            this.lblSoru14 = new System.Windows.Forms.Label();
            this.lblSoru4 = new System.Windows.Forms.Label();
            this.lblTeskkur = new System.Windows.Forms.Label();
            this.lblSoru5 = new System.Windows.Forms.Label();
            this.chckEvet4 = new System.Windows.Forms.CheckBox();
            this.chchHayır5 = new System.Windows.Forms.CheckBox();
            this.chckEvet5 = new System.Windows.Forms.CheckBox();
            this.chckHayır4 = new System.Windows.Forms.CheckBox();
            this.lablSoru15 = new System.Windows.Forms.Label();
            this.btnGonder = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblEgitmenAdi
            // 
            this.lblEgitmenAdi.AutoSize = true;
            this.lblEgitmenAdi.Location = new System.Drawing.Point(32, 28);
            this.lblEgitmenAdi.Name = "lblEgitmenAdi";
            this.lblEgitmenAdi.Size = new System.Drawing.Size(66, 13);
            this.lblEgitmenAdi.TabIndex = 0;
            this.lblEgitmenAdi.Text = "Egitmen Adı:";
            // 
            // lblEgitmenSoyadi
            // 
            this.lblEgitmenSoyadi.AutoSize = true;
            this.lblEgitmenSoyadi.Location = new System.Drawing.Point(30, 68);
            this.lblEgitmenSoyadi.Name = "lblEgitmenSoyadi";
            this.lblEgitmenSoyadi.Size = new System.Drawing.Size(84, 13);
            this.lblEgitmenSoyadi.TabIndex = 1;
            this.lblEgitmenSoyadi.Text = "Egitmen SoyAdı:";
            // 
            // lblSinif
            // 
            this.lblSinif.AutoSize = true;
            this.lblSinif.Location = new System.Drawing.Point(35, 298);
            this.lblSinif.Name = "lblSinif";
            this.lblSinif.Size = new System.Drawing.Size(30, 13);
            this.lblSinif.TabIndex = 2;
            this.lblSinif.Text = "Sınıf:";
            // 
            // llblbrans
            // 
            this.llblbrans.AutoSize = true;
            this.llblbrans.Location = new System.Drawing.Point(32, 145);
            this.llblbrans.Name = "llblbrans";
            this.llblbrans.Size = new System.Drawing.Size(37, 13);
            this.llblbrans.TabIndex = 3;
            this.llblbrans.Text = "Branş:";
            // 
            // lblYorum
            // 
            this.lblYorum.AutoSize = true;
            this.lblYorum.Location = new System.Drawing.Point(30, 180);
            this.lblYorum.Name = "lblYorum";
            this.lblYorum.Size = new System.Drawing.Size(82, 13);
            this.lblYorum.TabIndex = 4;
            this.lblYorum.Text = "Genel Yorumlar:";
            // 
            // cmbbrans
            // 
            this.cmbbrans.FormattingEnabled = true;
            this.cmbbrans.Location = new System.Drawing.Point(119, 137);
            this.cmbbrans.Name = "cmbbrans";
            this.cmbbrans.Size = new System.Drawing.Size(121, 21);
            this.cmbbrans.TabIndex = 7;
            // 
            // txtEgitmanAd
            // 
            this.txtEgitmanAd.Location = new System.Drawing.Point(120, 25);
            this.txtEgitmanAd.Name = "txtEgitmanAd";
            this.txtEgitmanAd.Size = new System.Drawing.Size(100, 20);
            this.txtEgitmanAd.TabIndex = 8;
            // 
            // txtEgitmenSoyadi
            // 
            this.txtEgitmenSoyadi.Location = new System.Drawing.Point(120, 64);
            this.txtEgitmenSoyadi.Name = "txtEgitmenSoyadi";
            this.txtEgitmenSoyadi.Size = new System.Drawing.Size(100, 20);
            this.txtEgitmenSoyadi.TabIndex = 9;
            // 
            // txtYorum
            // 
            this.txtYorum.Location = new System.Drawing.Point(118, 180);
            this.txtYorum.Multiline = true;
            this.txtYorum.Name = "txtYorum";
            this.txtYorum.Size = new System.Drawing.Size(122, 94);
            this.txtYorum.TabIndex = 10;
            // 
            // txtSinif
            // 
            this.txtSinif.Location = new System.Drawing.Point(119, 298);
            this.txtSinif.Name = "txtSinif";
            this.txtSinif.Size = new System.Drawing.Size(100, 20);
            this.txtSinif.TabIndex = 11;
            // 
            // lblSoru3
            // 
            this.lblSoru3.AutoSize = true;
            this.lblSoru3.Location = new System.Drawing.Point(287, 137);
            this.lblSoru3.Name = "lblSoru3";
            this.lblSoru3.Size = new System.Drawing.Size(173, 13);
            this.lblSoru3.TabIndex = 12;
            this.lblSoru3.Text = "Ders işleyişiden memnun musunuz?";
            // 
            // lblSoru11
            // 
            this.lblSoru11.AutoSize = true;
            this.lblSoru11.Location = new System.Drawing.Point(264, 25);
            this.lblSoru11.Name = "lblSoru11";
            this.lblSoru11.Size = new System.Drawing.Size(16, 13);
            this.lblSoru11.TabIndex = 13;
            this.lblSoru11.Text = "1)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(287, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Anlamadığınızda tekrar ediliyor mu?";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // lblSoru12
            // 
            this.lblSoru12.AutoSize = true;
            this.lblSoru12.Location = new System.Drawing.Point(265, 80);
            this.lblSoru12.Name = "lblSoru12";
            this.lblSoru12.Size = new System.Drawing.Size(16, 13);
            this.lblSoru12.TabIndex = 15;
            this.lblSoru12.Text = "2)";
            // 
            // chckEvet1
            // 
            this.chckEvet1.AutoSize = true;
            this.chckEvet1.Location = new System.Drawing.Point(580, 27);
            this.chckEvet1.Name = "chckEvet1";
            this.chckEvet1.Size = new System.Drawing.Size(48, 17);
            this.chckEvet1.TabIndex = 16;
            this.chckEvet1.Text = "Evet";
            this.chckEvet1.UseVisualStyleBackColor = true;
            // 
            // chckHayır1
            // 
            this.chckHayır1.AutoSize = true;
            this.chckHayır1.Location = new System.Drawing.Point(670, 27);
            this.chckHayır1.Name = "chckHayır1";
            this.chckHayır1.Size = new System.Drawing.Size(50, 17);
            this.chckHayır1.TabIndex = 17;
            this.chckHayır1.Text = "Hayır";
            this.chckHayır1.UseVisualStyleBackColor = true;
            // 
            // chckEvet2
            // 
            this.chckEvet2.AutoSize = true;
            this.chckEvet2.Location = new System.Drawing.Point(580, 79);
            this.chckEvet2.Name = "chckEvet2";
            this.chckEvet2.Size = new System.Drawing.Size(48, 17);
            this.chckEvet2.TabIndex = 18;
            this.chckEvet2.Text = "Evet";
            this.chckEvet2.UseVisualStyleBackColor = true;
            // 
            // chckHayır2
            // 
            this.chckHayır2.AutoSize = true;
            this.chckHayır2.Location = new System.Drawing.Point(670, 79);
            this.chckHayır2.Name = "chckHayır2";
            this.chckHayır2.Size = new System.Drawing.Size(50, 17);
            this.chckHayır2.TabIndex = 19;
            this.chckHayır2.Text = "Hayır";
            this.chckHayır2.UseVisualStyleBackColor = true;
            // 
            // chckEvet3
            // 
            this.chckEvet3.AutoSize = true;
            this.chckEvet3.Location = new System.Drawing.Point(580, 133);
            this.chckEvet3.Name = "chckEvet3";
            this.chckEvet3.Size = new System.Drawing.Size(48, 17);
            this.chckEvet3.TabIndex = 20;
            this.chckEvet3.Text = "Evet";
            this.chckEvet3.UseVisualStyleBackColor = true;
            // 
            // chckHayır3
            // 
            this.chckHayır3.AutoSize = true;
            this.chckHayır3.Location = new System.Drawing.Point(670, 133);
            this.chckHayır3.Name = "chckHayır3";
            this.chckHayır3.Size = new System.Drawing.Size(50, 17);
            this.chckHayır3.TabIndex = 21;
            this.chckHayır3.Text = "Hayır";
            this.chckHayır3.UseVisualStyleBackColor = true;
            // 
            // lblSoru13
            // 
            this.lblSoru13.AutoSize = true;
            this.lblSoru13.Location = new System.Drawing.Point(262, 137);
            this.lblSoru13.Name = "lblSoru13";
            this.lblSoru13.Size = new System.Drawing.Size(16, 13);
            this.lblSoru13.TabIndex = 22;
            this.lblSoru13.Text = "3)";
            // 
            // lblSoru1
            // 
            this.lblSoru1.AutoSize = true;
            this.lblSoru1.Location = new System.Drawing.Point(286, 25);
            this.lblSoru1.Name = "lblSoru1";
            this.lblSoru1.Size = new System.Drawing.Size(246, 13);
            this.lblSoru1.TabIndex = 23;
            this.lblSoru1.Text = "Öğretmeninizle sağlıklı iletiişim kurabiliyor musunuz?";
            // 
            // lblSoru14
            // 
            this.lblSoru14.AutoSize = true;
            this.lblSoru14.Location = new System.Drawing.Point(262, 191);
            this.lblSoru14.Name = "lblSoru14";
            this.lblSoru14.Size = new System.Drawing.Size(16, 13);
            this.lblSoru14.TabIndex = 24;
            this.lblSoru14.Text = "4)";
            // 
            // lblSoru4
            // 
            this.lblSoru4.AutoSize = true;
            this.lblSoru4.Location = new System.Drawing.Point(287, 191);
            this.lblSoru4.Name = "lblSoru4";
            this.lblSoru4.Size = new System.Drawing.Size(277, 13);
            this.lblSoru4.TabIndex = 25;
            this.lblSoru4.Text = "Öğretmeni sağladığı sosyal tesis ve hizmetler yeterlidir mi ?";
            // 
            // lblTeskkur
            // 
            this.lblTeskkur.AutoSize = true;
            this.lblTeskkur.Location = new System.Drawing.Point(547, 394);
            this.lblTeskkur.Name = "lblTeskkur";
            this.lblTeskkur.Size = new System.Drawing.Size(155, 13);
            this.lblTeskkur.TabIndex = 26;
            this.lblTeskkur.Text = "Katılımınız için teşekkür ederiz...";
            // 
            // lblSoru5
            // 
            this.lblSoru5.AutoSize = true;
            this.lblSoru5.Location = new System.Drawing.Point(287, 252);
            this.lblSoru5.Name = "lblSoru5";
            this.lblSoru5.Size = new System.Drawing.Size(224, 13);
            this.lblSoru5.TabIndex = 27;
            this.lblSoru5.Text = "Öğretmeniniz öğrenci işlerinde adil, tarafsız mı?";
            // 
            // chckEvet4
            // 
            this.chckEvet4.AutoSize = true;
            this.chckEvet4.Location = new System.Drawing.Point(580, 190);
            this.chckEvet4.Name = "chckEvet4";
            this.chckEvet4.Size = new System.Drawing.Size(48, 17);
            this.chckEvet4.TabIndex = 28;
            this.chckEvet4.Text = "Evet";
            this.chckEvet4.UseVisualStyleBackColor = true;
            // 
            // chchHayır5
            // 
            this.chchHayır5.AutoSize = true;
            this.chchHayır5.Location = new System.Drawing.Point(670, 257);
            this.chchHayır5.Name = "chchHayır5";
            this.chchHayır5.Size = new System.Drawing.Size(50, 17);
            this.chchHayır5.TabIndex = 29;
            this.chchHayır5.Text = "Hayır";
            this.chchHayır5.UseVisualStyleBackColor = true;
            // 
            // chckEvet5
            // 
            this.chckEvet5.AutoSize = true;
            this.chckEvet5.Location = new System.Drawing.Point(580, 257);
            this.chckEvet5.Name = "chckEvet5";
            this.chckEvet5.Size = new System.Drawing.Size(48, 17);
            this.chckEvet5.TabIndex = 30;
            this.chckEvet5.Text = "Evet";
            this.chckEvet5.UseVisualStyleBackColor = true;
            // 
            // chckHayır4
            // 
            this.chckHayır4.AutoSize = true;
            this.chckHayır4.Location = new System.Drawing.Point(670, 191);
            this.chckHayır4.Name = "chckHayır4";
            this.chckHayır4.Size = new System.Drawing.Size(50, 17);
            this.chckHayır4.TabIndex = 31;
            this.chckHayır4.Text = "Hayır";
            this.chckHayır4.UseVisualStyleBackColor = true;
            // 
            // lablSoru15
            // 
            this.lablSoru15.AutoSize = true;
            this.lablSoru15.Location = new System.Drawing.Point(262, 252);
            this.lablSoru15.Name = "lablSoru15";
            this.lablSoru15.Size = new System.Drawing.Size(16, 13);
            this.lablSoru15.TabIndex = 32;
            this.lablSoru15.Text = "5)";
            // 
            // btnGonder
            // 
            this.btnGonder.Location = new System.Drawing.Point(436, 335);
            this.btnGonder.Name = "btnGonder";
            this.btnGonder.Size = new System.Drawing.Size(75, 23);
            this.btnGonder.TabIndex = 33;
            this.btnGonder.Text = "Gönder";
            this.btnGonder.UseVisualStyleBackColor = true;
            this.btnGonder.Click += new System.EventHandler(this.btnGonder_Click);
            // 
            // EgitmenAnket
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(732, 461);
            this.Controls.Add(this.btnGonder);
            this.Controls.Add(this.lablSoru15);
            this.Controls.Add(this.chckHayır4);
            this.Controls.Add(this.chckEvet5);
            this.Controls.Add(this.chchHayır5);
            this.Controls.Add(this.chckEvet4);
            this.Controls.Add(this.lblSoru5);
            this.Controls.Add(this.lblTeskkur);
            this.Controls.Add(this.lblSoru4);
            this.Controls.Add(this.lblSoru14);
            this.Controls.Add(this.lblSoru1);
            this.Controls.Add(this.lblSoru13);
            this.Controls.Add(this.chckHayır3);
            this.Controls.Add(this.chckEvet3);
            this.Controls.Add(this.chckHayır2);
            this.Controls.Add(this.chckEvet2);
            this.Controls.Add(this.chckHayır1);
            this.Controls.Add(this.chckEvet1);
            this.Controls.Add(this.lblSoru12);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblSoru11);
            this.Controls.Add(this.lblSoru3);
            this.Controls.Add(this.txtSinif);
            this.Controls.Add(this.txtYorum);
            this.Controls.Add(this.txtEgitmenSoyadi);
            this.Controls.Add(this.txtEgitmanAd);
            this.Controls.Add(this.cmbbrans);
            this.Controls.Add(this.lblYorum);
            this.Controls.Add(this.llblbrans);
            this.Controls.Add(this.lblSinif);
            this.Controls.Add(this.lblEgitmenSoyadi);
            this.Controls.Add(this.lblEgitmenAdi);
            this.Name = "EgitmenAnket";
            this.Text = "EgitmenEkle";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblEgitmenAdi;
        private System.Windows.Forms.Label lblEgitmenSoyadi;
        private System.Windows.Forms.Label lblSinif;
        private System.Windows.Forms.Label llblbrans;
        private System.Windows.Forms.Label lblYorum;
        private System.Windows.Forms.ComboBox cmbbrans;
        private System.Windows.Forms.TextBox txtEgitmanAd;
        private System.Windows.Forms.TextBox txtEgitmenSoyadi;
        private System.Windows.Forms.TextBox txtYorum;
        private System.Windows.Forms.TextBox txtSinif;
        private System.Windows.Forms.Label lblSoru3;
        private System.Windows.Forms.Label lblSoru11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblSoru12;
        private System.Windows.Forms.CheckBox chckEvet1;
        private System.Windows.Forms.CheckBox chckHayır1;
        private System.Windows.Forms.CheckBox chckEvet2;
        private System.Windows.Forms.CheckBox chckHayır2;
        private System.Windows.Forms.CheckBox chckEvet3;
        private System.Windows.Forms.CheckBox chckHayır3;
        private System.Windows.Forms.Label lblSoru13;
        private System.Windows.Forms.Label lblSoru1;
        private System.Windows.Forms.Label lblSoru14;
        private System.Windows.Forms.Label lblSoru4;
        private System.Windows.Forms.Label lblTeskkur;
        private System.Windows.Forms.Label lblSoru5;
        private System.Windows.Forms.CheckBox chckEvet4;
        private System.Windows.Forms.CheckBox chchHayır5;
        private System.Windows.Forms.CheckBox chckEvet5;
        private System.Windows.Forms.CheckBox chckHayır4;
        private System.Windows.Forms.Label lablSoru15;
        private System.Windows.Forms.Button btnGonder;
    }
}