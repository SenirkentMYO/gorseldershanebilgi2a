﻿namespace GorselDershaneBilgi2A
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ogrbul = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.button5 = new System.Windows.Forms.Button();
            this.Txt_tcno = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.kuladi = new System.Windows.Forms.TextBox();
            this.sifre = new System.Windows.Forms.TextBox();
            this.btngiris = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.lblhata = new System.Windows.Forms.Label();
            this.Hata = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_ogrbul
            // 
            this.btn_ogrbul.Location = new System.Drawing.Point(97, 63);
            this.btn_ogrbul.Name = "btn_ogrbul";
            this.btn_ogrbul.Size = new System.Drawing.Size(75, 23);
            this.btn_ogrbul.TabIndex = 0;
            this.btn_ogrbul.Text = "BUL";
            this.btn_ogrbul.UseVisualStyleBackColor = true;
            this.btn_ogrbul.Click += new System.EventHandler(this.btn_ogrbul_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(203, 27);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(580, 372);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.button5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button5.Location = new System.Drawing.Point(-1, -2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(784, 33);
            this.button5.TabIndex = 2;
            this.button5.Text = "Öğrenci Bilgileri";
            this.button5.UseVisualStyleBackColor = false;
            // 
            // Txt_tcno
            // 
            this.Txt_tcno.Location = new System.Drawing.Point(82, 37);
            this.Txt_tcno.Name = "Txt_tcno";
            this.Txt_tcno.Size = new System.Drawing.Size(100, 20);
            this.Txt_tcno.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 40);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Öğrenci T.C =";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 170);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Kullanıcı Adı  =";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 216);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Şifre =";
            // 
            // kuladi
            // 
            this.kuladi.Location = new System.Drawing.Point(97, 167);
            this.kuladi.Name = "kuladi";
            this.kuladi.Size = new System.Drawing.Size(100, 20);
            this.kuladi.TabIndex = 7;
            // 
            // sifre
            // 
            this.sifre.Location = new System.Drawing.Point(97, 213);
            this.sifre.Name = "sifre";
            this.sifre.Size = new System.Drawing.Size(100, 20);
            this.sifre.TabIndex = 8;
            // 
            // btngiris
            // 
            this.btngiris.Location = new System.Drawing.Point(97, 256);
            this.btngiris.Name = "btngiris";
            this.btngiris.Size = new System.Drawing.Size(75, 23);
            this.btngiris.TabIndex = 9;
            this.btngiris.Text = "GİRİŞ";
            this.btngiris.UseVisualStyleBackColor = true;
            this.btngiris.Click += new System.EventHandler(this.btngiris_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(60, 139);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 13);
            this.label9.TabIndex = 10;
            this.label9.Text = "YETKİLİ GİRİŞİ";
            // 
            // lblhata
            // 
            this.lblhata.AutoSize = true;
            this.lblhata.Location = new System.Drawing.Point(609, 216);
            this.lblhata.Name = "lblhata";
            this.lblhata.Size = new System.Drawing.Size(0, 13);
            this.lblhata.TabIndex = 11;
            // 
            // Hata
            // 
            this.Hata.AutoSize = true;
            this.Hata.Location = new System.Drawing.Point(91, 256);
            this.Hata.Name = "Hata";
            this.Hata.Size = new System.Drawing.Size(0, 13);
            this.Hata.TabIndex = 12;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 99);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(0, 13);
            this.label10.TabIndex = 13;
            // 
            // Form1
            // 
            this.ClientSize = new System.Drawing.Size(782, 411);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.Hata);
            this.Controls.Add(this.lblhata);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btngiris);
            this.Controls.Add(this.sifre);
            this.Controls.Add(this.kuladi);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Txt_tcno);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.btn_ogrbul);
            this.Name = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        
        private System.Windows.Forms.Button btn_ogrbul;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox Txt_tcno;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox kuladi;
        private System.Windows.Forms.TextBox sifre;
        private System.Windows.Forms.Button btngiris;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblhata;
        private System.Windows.Forms.Label Hata;
        private System.Windows.Forms.Label label10;
    }
}

